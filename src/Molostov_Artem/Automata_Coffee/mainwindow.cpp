#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->progressBar->setVisible(false);
    ui->pb_1->setEnabled(false);
    ui->pb_2->setEnabled(false);
    ui->pb_5->setEnabled(false);
    ui->pb_10->setEnabled(false);
    ui->pb_50->setEnabled(false);
    ui->pb_100->setEnabled(false);
    ui->pb_prepare->setEnabled(false);
    ui->pb_change->setEnabled(false);
    ui->output->setEnabled(false);
    ui->rb_cappucino->setEnabled(false);
    ui->rb_coffee->setEnabled(false);
    ui->rb_espresso->setEnabled(false);
    ui->rb_hotchocolate->setEnabled(false);
    ui->rb_tea->setEnabled(false);
    ui->lcd_money->setEnabled(false);
}


MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_pb_1_clicked()
{
    ui->lcd_money->display(QString::number(MyCoffee.cash+1));
    MyCoffee.cash=ui->lcd_money->value();
}

void MainWindow::on_pb_2_clicked()
{
    ui->lcd_money->display(QString::number(MyCoffee.cash+2));
    MyCoffee.cash=ui->lcd_money->value();
}

void MainWindow::on_pb_5_clicked()
{
    ui->lcd_money->display(QString::number(MyCoffee.cash+5));
    MyCoffee.cash=ui->lcd_money->value();
}

void MainWindow::on_pb_10_clicked()
{
    ui->lcd_money->display(QString::number(MyCoffee.cash+10));
    MyCoffee.cash=ui->lcd_money->value();
}

void MainWindow::on_pb_50_clicked()
{
    ui->lcd_money->display(QString::number(MyCoffee.cash+50));
    MyCoffee.cash=ui->lcd_money->value();
}

void MainWindow::on_pb_100_clicked()
{
    ui->lcd_money->display(QString::number(MyCoffee.cash+100));
    MyCoffee.cash=ui->lcd_money->value();
}

void MainWindow::on_rb_coffee_clicked()       { MyCoffee.flag_prepare=1; }
void MainWindow::on_rb_tea_clicked()          { MyCoffee.flag_prepare=2; }
void MainWindow::on_rb_cappucino_clicked()    { MyCoffee.flag_prepare=3; }
void MainWindow::on_rb_hotchocolate_clicked() { MyCoffee.flag_prepare=4; }
void MainWindow::on_rb_espresso_clicked()     { MyCoffee.flag_prepare=5; }

void MainWindow::on_pb_prepare_clicked()
{
    switch (MyCoffee.flag_prepare)
    {
    case 1:
    {
        if ((MyCoffee.check_money(MyCoffee.coffee_price(),ui->lcd_money->value()))==true)
        {
            ui->progressBar->setVisible(true);
            for (long i=0;i<100000000;i++)
            ui->progressBar->setValue(i/1000000);
            ui->progressBar->setVisible(false);
            ui->progressBar->setValue(0);
            ui->output->setText("Enjoy your coffee!");
            ui->lcd_money->display(QString::number((ui->lcd_money->value())-(MyCoffee.coffee_price())));
        }
        else ui->output->setText("Not enough money! Add more please.");
        break;
    }
    case 2:
    {
        if ((MyCoffee.check_money(MyCoffee.tea_price(),ui->lcd_money->value()))==true)
        {
            ui->progressBar->setVisible(true);
            for (long i=0;i<100000000;i++)
            ui->progressBar->setValue(i/1000000);
            ui->progressBar->setVisible(false);
            ui->progressBar->setValue(0);
            ui->output->setText("Enjoy your tea!");
            ui->lcd_money->display(QString::number((ui->lcd_money->value())-(MyCoffee.tea_price())));

        }
        else ui->output->setText("Not enough money! Add more please");
        break;
    }
    case 3:
    {
        if ((MyCoffee.check_money(MyCoffee.cappucino_price(),ui->lcd_money->value()))==true)
        {
            ui->progressBar->setVisible(true);
            for (long i=0;i<100000000;i++)
            ui->progressBar->setValue(i/1000000);
            ui->progressBar->setVisible(false);
            ui->progressBar->setValue(0);
            ui->output->setText("Enjoy your cappucino!");
            ui->lcd_money->display(QString::number((ui->lcd_money->value())-(MyCoffee.cappucino_price())));
        }
        else ui->output->setText("Not enough money! Add more please");
        break;
    }
    case 4:
    {
        if ((MyCoffee.check_money(MyCoffee.hotchocolate_price(),ui->lcd_money->value()))==true)
        {
            ui->progressBar->setVisible(true);
            for (long i=0;i<100000000;i++)
            ui->progressBar->setValue(i/1000000);
            ui->progressBar->setVisible(false);
            ui->progressBar->setValue(0);
            ui->output->setText("Enjoy your hot chocolate!");
            ui->lcd_money->display(QString::number((ui->lcd_money->value())-(MyCoffee.hotchocolate_price())));
        }
        else ui->output->setText("Not enough money! Add more please");
        break;
    }
    case 5:
    {
        if ((MyCoffee.check_money(MyCoffee.espresso_price(),ui->lcd_money->value()))==true)
        {
            ui->progressBar->setVisible(true);
            for (long i=0;i<100000000;i++)
            ui->progressBar->setValue(i/1000000);
            ui->progressBar->setVisible(false);
            ui->progressBar->setValue(0);
            ui->output->setText("Enjoy your espresso!");
            ui->lcd_money->display(QString::number((ui->lcd_money->value())-(MyCoffee.espresso_price())));
        }
        else ui->output->setText("Not enough money! Add more please");
        break;
    }
    }

}

void MainWindow::on_pb_change_clicked()
{
    QString value_s = QString::number(ui->lcd_money->value());
    QString change = "Thank you! Your change is ";
    ui->output->setText(QString("%1 %2").arg(change).arg(value_s));
    ui->lcd_money->display(QString::number(0));
}


void MainWindow::on_powercheck_clicked(bool checked)
{
    if (checked==true) MyCoffee.on(); else MyCoffee.off();
    switch (MyCoffee.powervalue)
    {
    case false:
    {
    ui->pb_1->setEnabled(false);
    ui->pb_2->setEnabled(false);
    ui->pb_5->setEnabled(false);
    ui->pb_10->setEnabled(false);
    ui->pb_50->setEnabled(false);
    ui->pb_100->setEnabled(false);
    ui->pb_prepare->setEnabled(false);
    ui->pb_change->setEnabled(false);
    ui->output->setEnabled(false);
    ui->rb_cappucino->setEnabled(false);
    ui->rb_coffee->setEnabled(false);
    ui->rb_espresso->setEnabled(false);
    ui->rb_hotchocolate->setEnabled(false);
    ui->rb_tea->setEnabled(false);
    ui->lcd_money->setEnabled(false);
    }
    break;
    case true:
    {
        ui->pb_1->setEnabled(true);
        ui->pb_2->setEnabled(true);
        ui->pb_5->setEnabled(true);
        ui->pb_10->setEnabled(true);
        ui->pb_50->setEnabled(true);
        ui->pb_100->setEnabled(true);
        ui->pb_prepare->setEnabled(true);
        ui->pb_change->setEnabled(true);
        ui->output->setEnabled(true);
        ui->rb_cappucino->setEnabled(true);
        ui->rb_coffee->setEnabled(true);
        ui->rb_espresso->setEnabled(true);
        ui->rb_hotchocolate->setEnabled(true);
        ui->rb_tea->setEnabled(true);
        ui->lcd_money->setEnabled(true);
    }
    break;
    }
}
