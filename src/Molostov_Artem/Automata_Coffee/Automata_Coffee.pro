#-------------------------------------------------
#
# Project created by QtCreator 2016-04-19T19:52:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Automata_Coffee
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    coffee.h

FORMS    += mainwindow.ui
