#include "mainwindow.h"
#include "ui_mainwindow.h"
#define YEAR 31536000
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_today_clicked()
{
    MyDate.printToday();
    ui->le_day->setText(QString::number(MyDate.getDay()));
    ui->le_month->setText(QString::number(MyDate.getMonth()));
    ui->le_weekday->setText(MyDate.getWeekDay());
    ui->le_year->setText(QString::number(MyDate.getYear()));
    ui->le_min->setText(QString::number(MyDate.getMin()));
    ui->le_hour->setText(QString::number(MyDate.getHour()));
    ui->le_sec->setText(QString::number(MyDate.getSec()));
}

void MainWindow::on_pb_birthday_clicked()
{
    MyDate.printBirthDay();
    ui->le_day->setText(QString::number(MyDate.getDay()));
    ui->le_month->setText(QString::number(MyDate.getMonth()));
    ui->le_weekday->setText(MyDate.getWeekDay());
    ui->le_year->setText(QString::number(MyDate.getYear()));
    ui->le_min->setText(QString::number(MyDate.getMin()));
    ui->le_hour->setText(QString::number(MyDate.getHour()));
    ui->le_sec->setText(QString::number(MyDate.getSec()));

}

void MainWindow::on_pb_tommorow_clicked()
{
    MyDate.printTomorrow();
    ui->le_day->setText(QString::number(MyDate.getDay()));
    ui->le_month->setText(QString::number(MyDate.getMonth()));
    ui->le_weekday->setText(MyDate.getWeekDay());
    ui->le_year->setText(QString::number(MyDate.getYear()));
    ui->le_min->setText(QString::number(MyDate.getMin()));
    ui->le_hour->setText(QString::number(MyDate.getHour()));
    ui->le_sec->setText(QString::number(MyDate.getSec()));
}

void MainWindow::on_pb_yesterday_clicked()
{
    MyDate.printYesterday();
    ui->le_day->setText(QString::number(MyDate.getDay()));
    ui->le_month->setText(QString::number(MyDate.getMonth()));
    ui->le_weekday->setText(MyDate.getWeekDay());
    ui->le_year->setText(QString::number(MyDate.getYear()));
    ui->le_min->setText(QString::number(MyDate.getMin()));
    ui->le_hour->setText(QString::number(MyDate.getHour()));
    ui->le_sec->setText(QString::number(MyDate.getSec()));
}

void MainWindow::on_pb_future_clicked()
{
    QString number=ui->le_setday->text();
    MyDate.printFuture(number.toInt());
    ui->le_day->setText(QString::number(MyDate.getDay()));
    ui->le_month->setText(QString::number(MyDate.getMonth()));
    ui->le_weekday->setText(MyDate.getWeekDay());
    ui->le_year->setText(QString::number(MyDate.getYear()));
    ui->le_min->setText(QString::number(MyDate.getMin()));
    ui->le_hour->setText(QString::number(MyDate.getHour()));
    ui->le_sec->setText(QString::number(MyDate.getSec()));
}

void MainWindow::on_pb_past_clicked()
{
    QString number=ui->le_setday->text();
    MyDate.printPast(number.toInt());
    ui->le_day->setText(QString::number(MyDate.getDay()));
    ui->le_month->setText(QString::number(MyDate.getMonth()));
    ui->le_weekday->setText(MyDate.getWeekDay());
    ui->le_year->setText(QString::number(MyDate.getYear()));
    ui->le_min->setText(QString::number(MyDate.getMin()));
    ui->le_hour->setText(QString::number(MyDate.getHour()));
    ui->le_sec->setText(QString::number(MyDate.getSec()));

}

void MainWindow::on_pb_calcdiff_clicked()
{
    QString DateOneDay=ui->le_day->text();
    QString DateOneMonthDay=ui->le_month->text();
    QString DateOneYear=ui->le_year->text();
    QString DateOneHour=ui->le_hour->text();
    QString DateOneMin=ui->le_min->text();
    QString DateOneSec=ui->le_sec->text();
    QString DateTwoDay=ui->le_day_2->text();
    QString DateTwoMonthDay=ui->le_month_2->text();
    QString DateTwoYear=ui->le_year_2->text();
    QString DateTwoHour=ui->le_hour_2->text();
    QString DateTwoMin=ui->le_min_2->text();
    QString DateTwoSec=ui->le_sec_2->text();
    MyDate.calcDifference(
    DateOneDay.toInt(),DateOneMonthDay.toInt(),DateOneYear.toInt(),
    DateOneHour.toInt(), DateOneMin.toInt(),DateOneSec.toInt(),
    DateTwoDay.toInt(),DateTwoMonthDay.toInt(),DateTwoYear.toInt(),
    DateTwoHour.toInt(), DateTwoMin.toInt(),DateTwoSec.toInt());
    ui->le_daydiff->setText(QString::number(MyDate.getDay()));
    ui->le_mindiff->setText(QString::number(MyDate.getMin()));
    ui->le_hourdiff->setText(QString::number(MyDate.getHour()));
    ui->le_secdiff->setText(QString::number(MyDate.getSec()));
}

void MainWindow::on_pb_epoch_clicked()
{
    QString number=ui->le_epoch->text();
    MyDate.printEpoch(number.toULong());
    ui->le_day->setText(QString::number(MyDate.getDay()));
    ui->le_month->setText(QString::number(MyDate.getMonth()));
    ui->le_weekday->setText(MyDate.getWeekDay());
    ui->le_year->setText(QString::number(MyDate.getYear()));
    ui->le_min->setText(QString::number(MyDate.getMin()));
    ui->le_hour->setText(QString::number(MyDate.getHour()));
    ui->le_sec->setText(QString::number(MyDate.getSec()));
}

void MainWindow::on_pb_epoch_r_clicked()
{
    QString DateOneDay=ui->le_day->text();
    QString DateOneMonthDay=ui->le_month->text();
    QString DateOneYear=ui->le_year->text();
    QString DateOneHour=ui->le_hour->text();
    QString DateOneMin=ui->le_min->text();
    QString DateOneSec=ui->le_sec->text();
    ui->le_epoch->setText(QString::number(MyDate.RegtoEpoch(
    DateOneDay.toInt(),DateOneMonthDay.toInt(),DateOneYear.toInt(),
    DateOneHour.toInt(), DateOneMin.toInt(),DateOneSec.toInt())));
}
