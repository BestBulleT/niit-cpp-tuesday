#include "mainwindow.h"
#include "ui_mainwindow.h"
#define N 6378.1

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_answer_clicked()
{
    QString r=ui->le_add->text();
    MyCircle.setRadius(N);
    MyCircle.setFerence(r.toDouble()+(MyCircle.getFerence()));
    ui->le_space->setText(QString::number((MyCircle.getRadius())-(N)));

 }
