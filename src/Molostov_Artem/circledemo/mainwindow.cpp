#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_Radius_clicked()
{
    QString r=ui->le_Radius->text();
    MyCircle.setRadius(r.toDouble());
    ui->le_Ference->setText(QString::number(MyCircle.getFerence()));
    ui->le_Square->setText(QString::number(MyCircle.getSquare()));
}

void MainWindow::on_pb_Ference_clicked()
{
    QString r=ui->le_Ference->text();
    MyCircle.setFerence(r.toDouble());
    ui->le_Radius->setText(QString::number(MyCircle.getRadius()));
    ui->le_Square->setText(QString::number(MyCircle.getSquare()));

}

void MainWindow::on_pb_Square_clicked()
{
    QString r=ui->le_Square->text();
    MyCircle.setSquare(r.toDouble());
    ui->le_Radius->setText(QString::number(MyCircle.getRadius()));
    ui->le_Ference->setText(QString::number(MyCircle.getFerence()));

}
