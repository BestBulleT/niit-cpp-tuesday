#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "date.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pb_today_clicked();
    void on_pb_birthday_clicked();
    void on_pb_yesterday_clicked();
    void on_pb_tommorow_clicked();
    void on_pb_past_clicked();
    void on_pb_future_clicked();
    void on_pb_epoch_clicked();
    void on_pb_calcdiff_clicked();

    void on_pb_epoch_r_clicked();

private:
    Ui::MainWindow *ui;
    Date MyDate;
};

#endif // MAINWINDOW_H
