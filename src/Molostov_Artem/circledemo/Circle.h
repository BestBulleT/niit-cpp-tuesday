#ifndef CIRCLE_H
#define CIRCLE_H
#include "math.h"

class Circle
{
public:
    Circle():radius(0),ference(0),square(0){}
        double getRadius(){return radius;}
        double getFerence(){return ference;}
        double getSquare(){return square;}
        void setRadius(double r){
            radius=r;
            square=M_PI*r*r;
            ference=2*M_PI*r;
        }
        void setFerence(double f){
            radius=f/(2*M_PI);
            square=M_PI*(f/(2*M_PI))*(f/(2*M_PI));
            ference=f;
        }
        void setSquare(double s){
            radius=sqrt(s/M_PI);
            square=s;
            ference=2*M_PI*sqrt(s/M_PI);
        }

private:
    double radius,ference,square;
};



#endif // CIRCLE_H
