#-------------------------------------------------
#
# Project created by QtCreator 2016-04-05T18:10:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = circle2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    Circle.h

FORMS    += mainwindow.ui
