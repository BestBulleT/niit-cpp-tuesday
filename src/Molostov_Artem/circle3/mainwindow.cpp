#include "mainwindow.h"
#include "ui_mainwindow.h"
#define PRICE_ROAD 1000
#define PRICE_OUT 2000

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_calc_clicked()
{
    QString r=ui->le_input_pool->text();
    QString m=ui->le_input_road->text();
    MyCircle.setRadius((r.toDouble())+(m.toDouble()));
    ui->le_out->setText(QString::number((MyCircle.getFerence())*(PRICE_OUT)));
    double full=MyCircle.getSquare();
    MyCircle.setRadius(r.toDouble());
    ui->le_road->setText(QString::number(((full)-(MyCircle.getSquare()))*(PRICE_ROAD)));

}
