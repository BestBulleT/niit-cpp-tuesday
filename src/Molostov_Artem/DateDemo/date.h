#ifndef DATE_H
#define DATE_H
#define DAY_SEC 86400
#define HOUR_SEC 3600
#define MIN_SEC 60
#define CBD 773649000
#include <time.h>
typedef unsigned int UI;
class Date {
    struct tm * ptime;
    UI year, daymonth, day, weekday,hour,min,sec;
    time_t t;
    int diff=0,zero=0;
    char * weekdays[7] = {"Воскресенье", "Понедельник","Вторник",
                          "Среда","Четверг", "Пятница", "Суббота"};
public:
    void clear(){ zero=t; diff=0; }

    void printEpoch(unsigned long epoch)
    {
        time(&t);
        t=epoch;
        clear();
        ptime=localtime(&t);

    }
    int RegtoEpoch(int day1, int month1, int year1, int hour1, int min1, int sec1)
    {
       time(&t);
       ptime=localtime(&t);
       ptime->tm_year=year1-1900;
       ptime->tm_mon=month1-1;
       ptime->tm_mday=day1;
       ptime->tm_hour=hour1;
       ptime->tm_min=min1;
       ptime->tm_sec=sec1;
       zero=mktime(ptime);
       diff=0;
       return zero;
    }

    void printBirthDay()
    {
        time(&t);
        t=CBD;
        clear();
        ptime=localtime(&t);
    }
    
    void printToday()
    {
        time(&t);
        clear();
        ptime=localtime(&t);
    }
    void printYesterday()
    {

        printPast(1);
    }
    void printTomorrow()
    {

        printFuture(1);
    }

   void printFuture(int number)
   {
      time(&t);
      t=zero+DAY_SEC*number+diff;
      diff=t-zero;
      ptime=localtime(&t);
   }

   void printPast(int number)
   {
       time(&t);
       t=zero-DAY_SEC*number+diff;
       diff=t-zero;
       ptime=localtime(&t);
   }
    void calcDifference(int day1, int month1, int year1, int hour1, int min1, int sec1,
                        int day2, int month2, int year2, int hour2, int min2, int sec2)
    {
       int one=0, two=0;
       time(&t);
       ptime=localtime(&t);
       ptime->tm_year=year1-1900;
       ptime->tm_mon=month1-1;
       ptime->tm_mday=day1;
       ptime->tm_hour=hour1;
       ptime->tm_min=min1;
       ptime->tm_sec=sec1;
       one=mktime(ptime);
       ptime->tm_year=year2-1900;
       ptime->tm_mon=month2-1;
       ptime->tm_mday=day2;
       ptime->tm_hour=hour2;
       ptime->tm_min=min2;
       ptime->tm_sec=sec2;
       two=mktime(ptime);
       setdiff(two-one);
    }
    void setdiff(int value)
    {
        clear();
        if (value<0) { value=value-value-value; }
        if (value>=DAY_SEC)
        {
            ptime->tm_mday=(value/DAY_SEC);
            if (value%DAY_SEC>=0)
            ptime->tm_hour=((value-((ptime->tm_mday)*(DAY_SEC)))/(HOUR_SEC));
            if (value%HOUR_SEC>=0)
            ptime->tm_min=((value-(ptime->tm_mday*DAY_SEC+ptime->tm_hour*HOUR_SEC))/MIN_SEC);
            if (value%MIN_SEC>=0)
            ptime->tm_sec=(value-(ptime->tm_mday*DAY_SEC+ptime->tm_hour*HOUR_SEC+ptime->tm_min*MIN_SEC));
        }
        if ((value>=HOUR_SEC) && (value<DAY_SEC))
        {
            ptime->tm_mday=0;
            ptime->tm_hour=((value/HOUR_SEC));
            if (value%HOUR_SEC>=0)
            ptime->tm_min=((value-(ptime->tm_hour*HOUR_SEC))/MIN_SEC);
            if (value%MIN_SEC>=0)
            ptime->tm_sec=(value-(ptime->tm_hour*HOUR_SEC+ptime->tm_min*MIN_SEC));
        }
        if ((value>=MIN_SEC) && (value<HOUR_SEC) && (value<DAY_SEC))
        {
            ptime->tm_mday=0;
            ptime->tm_hour=0;
            ptime->tm_min=(value/MIN_SEC);
            if (value%MIN_SEC>=0)
            ptime->tm_sec=(value-(ptime->tm_min*MIN_SEC));
        }


    }

    int getYear(){year=ptime->tm_year;return year+1900;}
    int getDay(){day=ptime->tm_mday;return day;}
    int getHour(){hour=ptime->tm_hour;return hour;}
    int getMin(){min=ptime->tm_min;return min;}
    int getSec(){sec=ptime->tm_sec;return sec;}
    char * getWeekDay(){weekday=ptime->tm_wday;return weekdays[weekday];}
    int getMonth(){daymonth=ptime->tm_mon+1;return daymonth;}

};


#endif // DATE_H
