#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "coffee.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pb_1_clicked();

    void on_pb_2_clicked();

    void on_pb_5_clicked();

    void on_pb_10_clicked();

    void on_pb_50_clicked();

    void on_pb_100_clicked();

    void on_rb_coffee_clicked();

    void on_rb_tea_clicked();

    void on_rb_cappucino_clicked();

    void on_rb_hotchocolate_clicked();

    void on_rb_espresso_clicked();

    void on_pb_prepare_clicked();

    void on_pb_change_clicked();

    void on_powercheck_clicked(bool checked);

private:
    Ui::MainWindow *ui;
    Coffee MyCoffee;
};

#endif // MAINWINDOW_H
