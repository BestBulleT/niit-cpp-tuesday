#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Circle.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pb_Radius_clicked();
    void on_pb_Ference_clicked();
    void on_pb_Square_clicked();

private:
    Ui::MainWindow *ui;
    Circle MyCircle;
};

#endif // MAINWINDOW_H
