#-------------------------------------------------
#
# Project created by QtCreator 2016-04-09T03:33:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DateDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    coffee.cpp

HEADERS  += mainwindow.h \
    date.h

FORMS    += mainwindow.ui
