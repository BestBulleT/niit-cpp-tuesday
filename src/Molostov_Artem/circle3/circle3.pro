#-------------------------------------------------
#
# Project created by QtCreator 2016-04-05T18:34:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = circle3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    Circle.h

FORMS    += mainwindow.ui
